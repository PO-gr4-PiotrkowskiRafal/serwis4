function isEmpty(str) {
    return (!str || str.length === 0);
}


function validate(form) {
    checkEmail(form.elements[f_email],value, "niepoprawny email");
      checkString(form.elements["f_miasto"].value, "złe miasto");
    checkZIPCodeRegEx(["f_kod"].value );
}

function validate(form) {
    if (isEmpty(form.elements["f_miasto"].value)) {
        alert("Podaj miasto!");
        return false;
    }
    if (isEmpty(form.elements["f_email"].value)) {
        alert("Podaj email!");
        return false;
    }
    if (isEmpty(form.elements["f_miasto"].value)) {
        alert("Zaznacz zwierze !");
        return false;
    } else {
        return true;
    }
}


function isWhiteSpace(str) {
    var ws = "\t\n\r ";
    for (i = 0; i < str.length; i++) {
        var c = str.charAt(i);
        if (ws.indexOf(c) > -1) {
            return true;
        }
    }
    return false;
}

function checkString(str, msg) {
    if (isWhiteSpace(str) || isEmpty(str)) {
        alert(msg);
        return false;
    } else return true;
}

function checkEmail(str) {
    if (isWhiteSpace(str)) alert("Podaj właściwy e-mail"); else {
        at = str.indexOf("@");
        if (at < 1) {
            alert("Nieprawidłowy e-mail");
            return false;
        } else {
            var l = -1;
            for (var i = 0; i < str.length; i++) {
                var c = str.charAt(i);
                if (c == ".") l = i;
            }
            if ((l < (at + 2)) || (l == str.length - 1)) alert("Nieprawidłowy e-mail");
        }
        return true;
    }
}

function checkStringAndFocus(obj, msg) {
    var str = obj.value;
    var errorFieldName = "e_" + obj.name.substr(2, obj.name.length);
    if (isWhiteSpace(str) || isEmpty(str)) {
        document.getElementById(errorFieldName).innerHTML = msg;
        obj.focus();
        return false;
    } else return true;
}


errorField = "";

function startTimer(fName) {

    errorField = fName;

    window.setTimeout("clearError(errorField)", 5000);
}

function clearError(objName) {
    document.getElementById(objName).innerHTML = "";
}

function checkEmailRegEx(str) {
    var email = /[a-zA-Z_0-9\.]+@[a-zA-Z_0-9\.]+\.[a-zA-Z][a-zA-Z]+/;
    if (email.test(str)) return true; else alert("Podaj właściwy e-mail");
}

